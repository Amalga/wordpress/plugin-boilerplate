/**
 * Generates .pot
 * Duplicates .pot into .po for each defined language
 * Compiles .po file into .mo file.
 *
 * @since 1.0.8
 */
const wpPot = require('wp-pot'),
  fileSystem = require('fs'),
  package = require('../package.json'),
  gettextParser = require('gettext-parser'),
  destination = 'i18n/languages/',
  sourceFile = destination + package.name + '.pot',
  sourceFileContents = wpPot({
    destFile: sourceFile,
    domain: 't6-inv',
    package: 'Tier Six Inventory',
    src: '**/*.php',
    relativeTo: './',
    globOpts: {'nosort': true}
  }),
  languages = [
    'fr_CA'
  ]

createPoAndMo()

function createPoAndMo() {
  if (!sourceFileContents) {
    return
  }

  for (i = 0; i < languages.length; i++) {
    var targetFile = destination + package.name + '-' + languages[i] + '.po',
        moPath = destination + package.name + '-' + languages[i] + '.mo'
    if( ! targetFile ) {
      continue
    }

    generatePo( targetFile )
    generateMo(moPath, targetFile)
  }
}

function generatePo(targetFile) {
  try {
    fileSystem.appendFileSync(targetFile, sourceFileContents, { 'flag': 'ax' })
  } catch (error) {
    if (error.code) {
      console.log(
        `\x1b[47m\x1b[31mWARNING (NOT BAD)\x1b[0m \nNot overwriting the current .po file as it will remove any translations, this function is to generate the INITIAL .po file for new langauges and translations to be added to\nAnything added before running this script will be added and converted into the respective .mo file.\n\x1b[34m\x1b[41m^^\x1b[0m \x1b[37mREAD ME!!\x1b[0m`)
    } else {
      console.log('Error #: ', error.errno)
      console.log('Error Message: ', error.message)
    }
  }
}

function generateMo(moPath, poFile) {
  const input = fileSystem.readFileSync(poFile, 'utf-8'),
    po = gettextParser.po.parse(input),
    mo = gettextParser.mo.compile(po)

  fileSystem.writeFileSync(moPath, mo)
}
