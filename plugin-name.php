<?php
/**
 * Plugin Name: Plugin Name
 * Plugin URI: https://gitlab.com/Amalga/wordpress/plugin-boilerplate
 * Description: Short Plugin Description Here
 * Version: 0.0.1 (or use 1.0.0)
 * Author:  Personal/Company Name(s)
 * Author URI: https://Amalga.Network
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain: plugin-name
 * Domain Path: /languages
 *
 * @link https://Amalga.Network
 * @since 1.0.0
 * @package PluginName
 */

if ( version_compare( phpversion(), '5.6', '>=' ) ) {
	require_once __DIR__ . '/instance.php';
} else {
	if ( defined( 'WP_CLI' ) ) {
		WP_CLI::warning( _plugin_name_php_version_text() );
	} else {
		add_action( 'admin_notices', '_plugin_name_php_version_error' );
	}
}

/**
 * Admin notice for incompatible versions of PHP.
 */
function _plugin_name_php_version_error() {
	printf( '<div class="error"><p>%s</p></div>', esc_html( _plugin_name_php_version_text() ) );
}

/**
 * String describing the minimum PHP version.
 *
 * @return string
 */
function _plugin_name_php_version_text() {
	return __( 'Plugin Name plugin error: Your version of PHP is too old to run this plugin. You must be running PHP 5.6 or higher.', 'plugin-name' );
}
