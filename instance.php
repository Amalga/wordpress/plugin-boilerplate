<?php
/**
 * Instantiates the Plugin Name plugin
 *
 * @package PluginName
 */

namespace PluginName;

global $plugin_name_plugin;

require_once __DIR__ . '/php/class-plugin-base.php';
require_once __DIR__ . '/php/class-plugin.php';

$plugin_name_plugin = new Plugin();

/**
 * Plugin Name Plugin Instance
 *
 * @return Plugin
 */
function get_plugin_instance() {
	global $plugin_name_plugin;
	return $plugin_name_plugin;
}
