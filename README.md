# Amalga Network WordPress Plugin Boilerplate
A good starting point for using for WordPress plugins, a few things still need to be updated (such as the minimum WP & PHP versions). Lets consider this a sort of **WIP** for now.

## Setup
To get this going, clone* or zip the repo.
**If cloned make sure to remove or update your **git** reference unless submitting any merge requests*

### Find and Replace
The following items can be replaced to properly identify your plugin, make sure to do a case sensitive find/replace.
- `plugin-name`
- `plugin_name`
- `Plugin Name`
- `Plugin_Name`

### Rename Files
Rename the root file `plugin-name.php` to match your plugin name, which should match your plugins folder name.

During the find and replace you will also have updated the `class` **name** in `class-plugin-name.php`, rename this file to `class-<example-name>` where `<example-name>` would most likely be the same as the root file above.
