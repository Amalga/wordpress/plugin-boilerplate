<?php
/**
 * Front End (Main) logic for our plugin
 *
 * @package PluginName
 */

namespace PluginName;

/**
 * Class Plugin_Name
 *
 * @package PluginName
 */
class Plugin_Name {
	/**
	 * The plugin instance
	 *
	 * @var Plugin
	 */
	private $plugin;

	/**
	 * Class Constructor.
	 *
	 * @param object $plugin The plugin instance.
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
	}
}
