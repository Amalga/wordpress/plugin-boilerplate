<?php
/**
 * Admin for Plugin Name
 *
 * @package PluginName
 */

namespace PluginName;

/**
 * Class Admin
 *
 * @package PluginName
 */
class Admin {
	/**
	 * Plugin Options
	 */
	const MENU_PAGE_NAME = 'Plugin Name';

	/**
	 * Page Slug
	 */
	const MENU_PAGE_SLUG = 'plugin-name';

	/**
	 * User Level Required
	 */
	const CAPABILITIES = 'manage_options';

	/**
	 * Admin Scripts Handle
	 */
	const SCRIPTS_HANDLE = 'plugin-name-admin_js';

	/**
	 * WP option group
	 */
	const OPTION_GROUP = 'plugin-name';

	/**
	 * WP option name
	 */
	const OPTION_NAME = 'plugin-name';

	/**
	 * WP option name
	 */
	const NONCE_ACTION = 'plugin-name';

	/**
	 * Styles Handle
	 */
	const STYLES_HANDLE = 'plugin-name-css';

	/**
	 * The plugin instance
	 *
	 * @var Plugin
	 */
	private $plugin;

	/**
	 * The plugin options
	 *
	 * @var get_option
	 */
	private $options;

	/**
	 * Class Constructor.
	 *
	 * @param object $plugin The plugin instance.
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;

		add_action( 'admin_menu', [ $this, 'add_option_page' ] );
		add_action( 'admin_init', [ $this, 'init' ] );
	}

	/**
	 * Initiate the plugin resources.
	 *
	 * @action after_setup_theme
	 */
	public function init() {
		add_action( 'admin_enqueue_scripts', [ $this, 'register_admin_assets' ] );
	}

	/**
	 * Create menus
	 *
	 * @action admin_menu
	 *
	 * @return void
	 */
	public function add_option_page() {
		add_options_page(
			self::MENU_PAGE_NAME,
			self::MENU_PAGE_NAME,
			self::CAPABILITIES,
			self::MENU_PAGE_SLUG,
			[ $this, 'page_output' ]
		);
	}

	/**
	 * Register scripts and stylesheets.
	 *
	 * @action admin_enqueue_scripts
	 */
	public function register_admin_assets() {
	}

	/**
	 * Outputs the HTML :)
	 */
	public function page_output() {
		require $this->plugin->dir_path . 'admin-output.php';
	}
}
