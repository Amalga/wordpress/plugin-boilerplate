<?php
/**
 * Bootstraps the plugin.
 *
 * @package PluginName
 */

namespace PluginName;

/**
 * Class Plugin
 *
 * @package PluginName
 */
class Plugin extends Plugin_Base {
	/**
	 * The plugin instance
	 *
	 * @var Plugin
	 */
	public $plugin;

	/**
	 *  Admin object
	 *
	 * @var Admin
	 */
	public $admin;

	/**
	 * Class constructor.
	 */
	public function __construct() {
		parent::__construct();

		$priority = 9; // Because WP_Customize_Widgets::register_settings() happens at after_setup_theme priority 10.
		add_action( 'after_setup_theme', [ $this, 'init' ], $priority );
		add_action( 'plugins_loaded', [ $this, 'load_languages' ] );
		add_filter( 'timber_locations', [ $this, 'filter_timber_locations' ] );
	}

	/**
	 * Initiate the plugin resources.
	 *
	 * @action after_setup_theme
	 */
	public function init() {
		$this->admin = new Admin( $this );
	}

	/**
	 * Load Languages
	 *
	 * Loads the language files for the plugin
	 */
	public function load_languages() {
		load_plugin_textdomain( 'plugin-name', false, trailingslashit( dirname( dirname( plugin_basename( __FILE__ ) ) ) ) . 'languages/' );
	}
}
